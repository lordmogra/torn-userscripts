// ==UserScript==
// @name         Find Offliners With RW Weapons
// @namespace    http://mogsrealm.com/
// @version      2024-04-01
// @description  Retrieve those unused weapons!
// @author       LordMogra
// @match        *.torn.com/factions.php?step=your*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=torn.com
// @updateURL    https://gitlab.com/lordmogra/torn-userscripts/-/raw/main/findofflinerswithrw.user.js
// @downloadURL  https://gitlab.com/lordmogra/torn-userscripts/-/raw/main/findofflinerswithrw.user.js
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_registerMenuCommand
// ==/UserScript==

(function($) {
    'use strict';

    let api_key = '###PDA_APIKEY###';

    if (api_key.charAt(0) === "#")
    {
        const config_key = 'FindOfflinersWithRW_api_key'
        GM_registerMenuCommand('Set API Key', () => {
            api_key = prompt('Public Access API Key', api_key);
            GM_setValue(config_key, api_key);
        });

        api_key = GM_getValue(config_key);
    }
    function waitForElm(selector) {
        return new Promise(resolve => {
            if (document.querySelector(selector)) {
                return resolve(document.querySelector(selector));
            }

            const observer = new MutationObserver(mutations => {
                if (document.querySelector(selector)) {
                    observer.disconnect();
                    resolve(document.querySelector(selector));
                }
            });

            // If you get "parameter 1 is not of type 'Node'" error, see https://stackoverflow.com/a/77855838/492336
            observer.observe(document.body, {
                childList: true,
                subtree: true
            });
        });
    }

    function checkInWeaponsArmoury(event)
    {
        const inArmoury = window.location.hash.match(/tab=armoury/);
        const inWeapons = window.location.hash.match(/sub=weapons/);
        if (inArmoury && inWeapons)
        {
            waitForElm('#armoury-weapons ul.item-list').then((elm) => {
                const weaponsArmouryItemList = $(elm);
                const weapons = weaponsArmouryItemList.children('li');

                function checkWeaponHasBonus(weapon)
                {
                    return weapon.find('.bonus > i:not(.bonus-attachment-blank-bonus-25)').length > 0;
                }

                function checkWeaponLesseeActive(weapon)
                {
                    const lesseeLink = weapon.find('.loaned > a').get(0);
                    if (lesseeLink)
                    {
                        const lesseeId = lesseeLink.href.match(/[0-9]+/)[0];
                        $.ajaxSetup({async:false});
                        let isActive = JSON.parse($.get(`https://api.torn.com/user/${lesseeId}`,
                        {
                            key: api_key,
                        }).responseText).last_action.status !== 'Offline';
                        return isActive;
                    }
                    else
                    {
                        //No Lessee
                        return true;
                    }
                }

                weapons.each(function (i){
                    const weapon = $(this);
                    const bonuses = weapon.find('.bonus').children();
                    if (checkWeaponHasBonus(weapon)&& !checkWeaponLesseeActive(weapon))
                    {
                        weapon.css('background-color', 'rgba(255,0,0,.5)');
                    }
                });
            });
        }
    }
    window.addEventListener("hashchange", checkInWeaponsArmoury);
    checkInWeaponsArmoury();
})(jQuery);