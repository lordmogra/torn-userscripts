// ==UserScript==
// @name         Torn Train Lowest
// @namespace    http://mogsrealm.com/
// @version      2024-09-20
// @description  Train the lowest stat in Torn
// @author       LordMogra
// @match        https://www.torn.com/gym.php
// @icon         https://www.google.com/s2/favicons?domain=torn.com
// @updateURL    https://gitlab.com/lordmogra/torn-userscripts/-/raw/main/trainlowest.user.js
// @downloadURL  https://gitlab.com/lordmogra/torn-userscripts/-/raw/main/trainlowest.user.js
// @grant        none
// ==/UserScript==

(() =>
{
  'use strict';

  (function(e,f){var b={},g=function(a){b[a]&&(f.clearInterval(b[a]),b[a]=null)};e.fn.waitUntilExists=function(a,h,j){var c=this.selector,d=e(c),k=d.not(function(){return e(this).data("waitUntilExists.found")});"remove"===a?g(c):(k.each(a).data("waitUntilExists.found",!0),h&&d.length?g(c):j||(b[c]=f.setInterval(function(){d.waitUntilExists(a,h,!0)},500)));return d}})(jQuery,window);

  var castToNumber = (value) => parseFloat(value.replaceAll(',', ''));

  const trainLowest = $('<button />');
  trainLowest.text("TRAIN LOWEST");
  trainLowest.addClass('torn-btn');

  trainLowest.click(() =>
  {
    var lowest;

    [
      {
        name: 'STR',
        value: castToNumber($('li[class^="strength_"] span[class^="propertyValue_"]').text()),
        button: $('li[class^="strength_"] button.torn-btn')
      },
      {
        name: 'DEF',
        value: castToNumber($('li[class^="defense_"] span[class^="propertyValue_"]').text()),
        button: $('li[class^="defense_"] button.torn-btn')
      },
      {
        name: 'SPD',
        value: castToNumber($('li[class^="speed_"] span[class^="propertyValue_"]').text()),
        button: $('li[class^="speed_"] button.torn-btn')
      },
      {
        name: 'DEX',
        value: castToNumber($('li[class^="dexterity_"] span[class^="propertyValue_"]').text()),
        button: $('li[class^="dexterity_"] button.torn-btn')
      }
    ].filter((stat) => !stat.button[0].disabled
                    && stat.button.parents('.tt-gym-locked').length == 0
    ).forEach((stat) =>
    {
      if (typeof lowest === 'undefined' || stat.value < lowest.value)
      {
        lowest = stat;
      }
    });

    lowest?.button.click();
  });

  $('div[class^="gymContent_"]').waitUntilExists((event, xhr, settings) =>
  {
    const gymContent = $('div[class^="gymContent_"]');
    trainLowest.appendTo($('<center />').appendTo(gymContent));
  });
})();
